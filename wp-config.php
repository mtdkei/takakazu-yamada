<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'takakazu-yamada');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'root');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'root');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|4?+VM^:?>B|YvYPwIJKAp$MPFlCpHhWaGxg5WRm;*P5 ,&mZS@Ct&r}?c`3O*dq');
define('SECURE_AUTH_KEY',  'dJc+S8KSh|`aA)gIO0sH5LN&a$g)q2a,},Z?8pN-U[Dy86,oKW!/-#-n-9%#v[O{');
define('LOGGED_IN_KEY',    'gF?yLTwx>?dl1T1e}mU0g*f4JNwLy%tIWnU|l@Nr>w}JGg,nt2^7gkrl?(xo@O3|');
define('NONCE_KEY',        'W{mtGRDcnwzVMP6/8nfVy6#K?D1i/=FifzL+AO||wy~,=>(;B|zw_/<-vvWv`,y-');
define('AUTH_SALT',        'w1vp+<>GxrSVCH#@OC6+CI,V:ril.JTFY3C/qYg@m!n:LjdTA?13d D{1G^f+Oa2');
define('SECURE_AUTH_SALT', 'k-iXt8q]9kGy-0h#+q<U2{vk>TL7Tw+6M@!`FsA1%8L+[m|emC$kom=q#0[@5+rE');
define('LOGGED_IN_SALT',   'cS-oh^3,WJ~!B,=f?=q>8*-dMqjUp;Bka$ZWpZioCR1LZW_]SOOwl]N[{RvTh(eZ');
define('NONCE_SALT',       'vnF#3lBRuh91c0&mq#QVi*m8;CifW:jc* !}b@FDA2Q^T3bKTeT17WxX#45-b{|G');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
